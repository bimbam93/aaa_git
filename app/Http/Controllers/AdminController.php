<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\CreateUserRequest;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\User;
use Carbon\Carbon;

//php artisan make:controller AdminController
class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('auth')->except(['login', 'loginPost']);
    }
    
    public function login(Request $req){
        return view('login');
    }

    // php artisan make:request LoginRequest
    public function loginPost(LoginRequest $req){
        //validated

        if(Auth::attempt([
            'email' => $req->email, 
            'password' => $req->password, 
            'active'=> 1
            ])){

                $req->session()->regenerate();
                return redirect('dashboard');
            }

            return back()->withErrors([
                'email' => 'Az adott paraméterekkel nincs felhasználó a rendszerben.'
            ]);
    }

    public function logout(Request $req){
        Auth::logout();
        $req->session()->invalidate();
        $req->session()->regenerateToken();

        return redirect('/');
    }

    public function dashboard(Request $req){
        return view('dashboard', ['users'=>User::all()]);
    }

    //php artisan make:request CreateUserRequest
    public function create_user(CreateUserRequest $req){
        //validated

        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);

        $user->email_verified_at = Carbon::now();
        $user->active = 1;
        
        $user->save();

        $req->session()->flash('msg', 'Felhasználó létrehozva: '.$user->name.'(id:'.$user->id.')');

        return redirect('dashboard');
    }
}
