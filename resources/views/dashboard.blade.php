Dashboard - {{ Auth::user()->name }}

<a href="{{ route('logout') }}">Kilépés</a>

<hr>

@if(Session::has('msg'))
    {{ Session::get('msg') }}
@endif

<hr>

@if(Auth::user()->role == "admin")

Felhasználó létrehozása
<form action="{{  route('create_user') }}" method="POST">
    @csrf

    Név:
    <input type="text" name="name"/>
    <br>
    Email: 
    <input type="email" name="email"/>
    <br>
    Jelszó
    <input type="password" name="password"/>
    <br>
    <input type="submit" value="Létrehozás">
</form>

<hr>
@endif

Felhasználók:
<ul>
@foreach ($users as $u)
    <li>{{ $u->name }}</li>
@endforeach

</ul>