<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;


use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [AdminController::class, 'login'])->name('login');
Route::post('/login', [AdminController::class, 'loginPost']);

Route::get('/logout', [AdminController::class, 'logout'])->name('logout');

Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');

Route::post('/create/user', [AdminController::class, 'create_user'])->name('create_user');


Route::get('/debug', function(){
    Log::debug('Ez egy debug üzenet..',['user_id'=>1, 'alma'=>'körte']);
    //Log::info('Ez egy info üzenet..');
    Log::warning('Ez egy warning üzenet..');
    Log::error('Ez egy error üzenet..');
    Log::emergency('Ez egy emergency üzenet..');
    
    return 'debug';
})->middleware('auth')->name('debug');

Route::get('/session', function(Request $req){

    var_dump($req->session()->all());

});

Route::get('/sessionadd', function(Request $r){

    $r->session()->put('user_id', 11);
    echo 'session added';

});

Route::get('/sessionflash', function(Request $r){

    $r->session()->flash('msg_type', 'danger');
    $r->session()->flash('msg', 'Üzenet');

});

Route::get('/sessionrem', function(Request $r){

    $id = $r->session()->pull('user_id', -1);
    echo 'sessino removed id:'.$id;

});


